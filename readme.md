Компонент вcnавляет блок кода для картинок,
В шаблоне компонента можно посмотреть структуру.

Версия сырая на данный момент. Внимательно смотрите исходя из проекта,
также следите, чтобы шаблон подходил к запросам фронта
для исключения чего-то лишнего внутри тега picture, source, img.

Пример вызова компонента:

```php
<?
$APPLICATION->IncludeComponent(
    'custom:picture',
    '',
    [
        'IMG_ID' => SiteOptions::getValue("aboutVideoIMG"), // ID картинки из админки 
        'SIZES' => [ // пишем размеры как нам написал фронт
            '1024px' => ['width' => 920, 'height' => 550, 'min' => true], // bool "min" - указатель для формирования min/max-width
            'original' => ['width' => 2350, 'height' => 810, 'min' => true] // размер для оригинала,который в теге img
        ],
        'PICTURE_CLASS' => '', // Для класса тега picture, если требуется
        'IMG_CLASS' => 'photo lazy', // Для класса тега img, если требуется
        'IMG_ALT' => 'solutions for global healthcare market', // Для alt тега img
        'IMG_TITLE' => 'solutions for global healthcare market', // Для title тега img
        'EXTRA_OPTIONS' =>  'lazy="true"', // Доп параметры
        'LAZY' => 'false/true',//лэзи
        'CACHE_TIME' => '', // задаем время кеширования, пустое- стандартное значение
    ],
    false
);
?>
```