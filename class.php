<?

use CFile;

class CustomPicture
{
    const ORIGINAL_IMAGE_ROOT = "upload/";
    const WEBP_IMAGE_ROOT = "upload/resize_webp/";

    public static function getAspectRatioSize(int $ID, int $screenWidth)
    {
        $image = CFile::GetFileArray($ID);
        $aspect_ratio = $image['WIDTH'] / $image['HEIGHT'];

        return $screenWidth / $aspect_ratio; // высота
    }

    public static function getSoures(int $imageID, array $arSizes)
    {
        foreach ($arSizes as $formatKey => $arFormat) {
            foreach ($arFormat as $mediaKey => $arSize) {
                $src = CustomPicture::GetResponsive($imageID, $arSize);
                $sources['webp'][$formatKey][$mediaKey]['src'] = CustomPicture::getWebP($src);
                $sources['default'][$formatKey][$mediaKey]['src'] = $src;
            }
        }
        return $sources;
    }

    public static function GetResponsive(int $id, array $arSize): string
    {
        $result = '';
        if ($id) {
            $arFile = \CFile::ResizeImageGet($id, $arSize, \BX_RESIZE_IMAGE_PROPORTIONAL);
            if ($arFile['src'])
                $result = $arFile['src'];
        }
        return $result;
    }

    static protected function createPathToWebp(string $originalFilePath): string
    {
        $path = \str_replace(static::ORIGINAL_IMAGE_ROOT, static::WEBP_IMAGE_ROOT, $originalFilePath);
        $dotPos = strripos($path, ".");
        $path = substr($path, 0, $dotPos + 1);
        $path .= "webp";
        return $path;
    }

    /**
     * Получение пути до webp-копии изображения
     *
     * @param string $originalFilePath Путь до оригинального изображения
     *
     * @return string Путь до webp-копии, строка пустая если копии нет.
     */
    public static function getWebP(string $originalFilePath): string
    {
        $webpSrc = static::createPathToWebp($originalFilePath);

        if (!\file_exists($_SERVER["DOCUMENT_ROOT"] . $webpSrc)) {
            $webpSrc = "";
        }

        return $webpSrc;
    }
}
