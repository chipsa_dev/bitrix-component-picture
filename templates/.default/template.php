<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

?>
<picture class="<?= $arParams['PICTURE_CLASS'] ?>">
	<? foreach ($arResult as $typeKey => $arType) : ?>
		<? foreach ($arType as $formatKey => $arFormat) : ?>
			<? foreach ($arFormat as $size => $source) : ?>
				<? if (!$source["src"]) continue; ?>
				<? if ($size == 'original' && $typeKey == "webp") : ?>
					<source type="image/webp" <?= $arParams['LAZY'] ? 'data-' : '' ?>srcset="<?= $source["src"] ?>">
				<? elseif ($size != 'original') : ?>
					<source <?= $typeKey == "webp" ? "type = \"image/webp\"" : ""; ?>media="(<?= $formatKey ?>-width: <?= $size ?>)" <?= $arParams['LAZY'] ? 'data-' : '' ?>srcset="<?= $source["src"] ?>">
				<? endif; ?>
			<? endforeach; ?>
		<? endforeach; ?>
	<? endforeach; ?>
	<img <?= $arParams['LAZY'] ? 'data-' : '' ?>src="<?= $arResult["default"]["original"]["original"]["src"] ?>" class="<?= $arParams['IMG_CLASS'] ?>" <?= $arParams['EXTRA_OPTIONS'] ?> title="<?= $arParams['IMG_TITLE'] ?>" alt="<?= $arParams['IMG_ALT'] ?>" width="<?= $arParams['SIZES']["original"]['width'] ?>" height="<?= $arParams['SIZES']["original"]['height'] ?>">
</picture>
