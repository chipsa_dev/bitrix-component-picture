<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
if (!isset($arParams["CACHE_TIME"]))
    $arParams["CACHE_TIME"] = 36000000;

if (!empty($arParams['IMG_ID'])) {
    foreach ($arParams['SIZES'] as $keyScreen => &$sizes) {
        if (empty($sizes["height"])) {
            $sizes["height"] = intval(CustomPicture::getAspectRatioSize($arParams['IMG_ID'], $sizes["width"]));
        }
        if ($keyScreen == "original") {
            unset($sizes["min"]);
            $arSizes['original'][$keyScreen] = $sizes;
        } else {
            if ($sizes["min"]) {
                unset($sizes["min"]);
                $arSizes['min'][$keyScreen] = $sizes;
            } else {
                unset($sizes["min"]);
                $arSizes['max'][$keyScreen] = $sizes;
            }
        }
    }

    if ($this->startResultCache()) {
        if (is_array($arSizes))
            $arResult = CustomPicture::getSoures($arParams['IMG_ID'], $arSizes);
        $this->includeComponentTemplate();
    }
}
